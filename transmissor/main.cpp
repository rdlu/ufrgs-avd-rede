#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <cstring>
#include <time.h>
#include <sys/time.h>

 
#define PORT 3456
 
int main(int argc, char *argv[]){
   int fd1, fd2;
   int num_b;
   int len_msg;
 
   
 
   struct sockaddr_in client;
   struct sockaddr_in server;
   struct hostent *host;
 
   if(argc != 4){
      printf("%s - faltam parametros: <host> <tamanho do pacote em Bytes> <taxa em kbps>\n", argv[0]);
      exit(1);
   }
 
   host = gethostbyname(argv[1]);
   int tamanho = atoi(argv[2]);
   char msg[tamanho];
   int taxa_kbps = atoi(argv[3]);
   int taxa_bps = taxa_kbps * 1024;
   
   //calcula a taxa de pacotes
   int bits_por_pacote = tamanho * 8;
   int pkts_por_segundo = taxa_bps / bits_por_pacote;
   int total_pkts = pkts_por_segundo * 5;
   
   timespec nanosleep_time;
   nanosleep_time.tv_nsec = 1000000000 / pkts_por_segundo;
   nanosleep_time.tv_sec = 0;
   
   
 
   if(host == NULL){
      printf("Impossivel resolver endereco do servidor...\n");
      exit(1);
   }
 
 
   fd1 = socket(AF_INET, SOCK_DGRAM, 0);
 
   if(fd1 == -1){
      perror("socket");
      exit(1);
   }
 
   client.sin_family = AF_INET;
   client.sin_port = htons(PORT);
   client.sin_addr.s_addr = INADDR_ANY;
    
   server.sin_family = host->h_addrtype;
   server.sin_port = htons(PORT);
   memcpy((char *) &server.sin_addr.s_addr, host->h_addr_list[0], host->h_length);
 
   bzero(&(client.sin_zero), 8);
   bzero(&(server.sin_zero), 8);
 
   fd2 = connect(fd1, (struct sockaddr *)&server, sizeof(struct sockaddr));
 
   if(fd2 == -1){
      perror("connect");
      exit(1);
   }
    
   memset(msg, 0x0, 100);
   struct timeval t0;
   sprintf(msg,"%d",total_pkts);
   len_msg = sizeof(msg);
   sendto(fd1, msg, len_msg, 0, (struct sockaddr *)&server, sizeof(struct sockaddr));
   
   for(int i = 0; i < total_pkts; i++) {
      gettimeofday(&t0, 0);
      sprintf(msg,"%ld.%ld",t0.tv_sec,t0.tv_usec);
       
      len_msg = sizeof(msg);
      sendto(fd1, msg, len_msg, 0, (struct sockaddr *)&server, sizeof(struct sockaddr));
      
      nanosleep(&nanosleep_time,&nanosleep_time);
   }
   
   sprintf(msg,"exit");
   len_msg = sizeof(msg);
   sendto(fd1, msg, len_msg, 0, (struct sockaddr *)&server, sizeof(struct sockaddr));
   
   shutdown(fd1,SHUT_RDWR);
   shutdown(fd2,SHUT_RDWR);
}
 