/* 
 * File:   main.cpp
 * Author: rodrigo
 * RECEPTOR
 * Created on December 29, 2012, 1:51 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <cstring>
#include <arpa/inet.h>
#include <time.h>
#include <sys/time.h>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>

#define PORT 3456
#define MAX_CONNECTS_TRY 10


long int time_diff(struct timeval x , struct timeval y)
{
    long int x_ms , y_ms , diff;
     
    x_ms = (long int)x.tv_sec*1000000 + (long int)x.tv_usec;
    y_ms = (long int)y.tv_sec*1000000 + (long int)y.tv_usec;
     
    diff = (long int)y_ms - (long int)x_ms;
     
    return diff;
}

int main(int argc, char *argv[]) {

    int fd1;
    int num_b;
    int bind_return;
    int listen_return;
    int sin_size;
    int msg_size;
	int count=0;
	std::vector<struct timeval> timeStampsRec;
	std::vector<std::string> timeStampsTrans;

    char msg[1600];

    struct sockaddr_in client;
    struct sockaddr_in host;
	struct timeval t0;
	int i;

  fd1 = socket(AF_INET, SOCK_DGRAM, 0);

    if (fd1 == -1) {
        perror("socket");
        exit(1);
    }

    client.sin_family = AF_INET;
    client.sin_port = htons(PORT);
    client.sin_addr.s_addr = INADDR_ANY;

    bind_return = bind(fd1, (struct sockaddr *) &client, sizeof (struct sockaddr));

    if (bind_return == -1) {
        perror("bind");
        exit(1);
    }
	memset(msg, 0x0, 1600);
        sin_size = sizeof (host);
        msg_size = recvfrom(fd1, msg, 1600, 0, (struct sockaddr *) &host, (socklen_t*) &sin_size);
	gettimeofday(&t0, 0);	
	int pcktNumber;
	sscanf(msg, "%d", &pcktNumber);
	pcktNumber = pcktNumber + 2;

	timeStampsRec.reserve(pcktNumber);
	timeStampsTrans.reserve(pcktNumber);

	struct timeval tBetween;
    while (strcmp("exit", msg) != 0) {
        memset(msg, 0x0, 1600);
        sin_size = sizeof (host);
        msg_size = recvfrom(fd1, msg, 1600, 0, (struct sockaddr *) &host, (socklen_t*) &sin_size);
	gettimeofday(&tBetween, 0);
	count++;
	timeStampsTrans.push_back(msg);
	timeStampsRec.push_back(tBetween);
//        printf("Mensagem de:%s: %s \n", inet_ntoa(host.sin_addr), msg);

    }

	//get time after receiving all packets
	struct timeval tFinal;
	gettimeofday(&tFinal, 0);

	//Diff between inicial and final time
	long int totalRecTime = time_diff(t0, tFinal);
	float totalRecTimeSec = (float)totalRecTime/1000000;
	printf("Total time for receiving %d packets is %f seconds\n\n", count, totalRecTimeSec);

	//in seconds 
	float realTimeSec = totalRecTime/1000000;
	//in bits
	float totalSizeRec = ((msg_size * count)*8);

	float actualRate = totalSizeRec/realTimeSec;
	float packtsPerSec = count/realTimeSec;
	printf("Actual rate is %f Kbits/s and %f pckts/s\n\n", actualRate/1000, packtsPerSec);
	
	

	//Diff between received and expected packets
	count = count + 1;
	printf("Packets received is: %d and the number of expected packets is: %d\n\n", count, pcktNumber);
	printf("%d packets were lost\n\n", pcktNumber - count);

	int countDeltas=0;
	for(int k=0; k<timeStampsRec.size()-1; ++k){
//		printf("tempo local: %ld.%ld msg: %d ", timeStampsRec[k].tv_sec, timeStampsRec[k].tv_usec, k);
//		std::cout << "tempo recebido: " << timeStampsTrans[k] << std::endl;
		
		//split string to convert from string to time
		std::stringstream ss(timeStampsTrans[k]);
    		std::string item;
		struct timeval transTime;
    		std::getline(ss, item, '.');
		long int auxTime;
		sscanf(item.c_str(), "%ld", &auxTime);
		transTime.tv_sec = auxTime;
		std::getline(ss, item, '.');
		sscanf(item.c_str(), "%ld", &auxTime);
		transTime.tv_usec = auxTime;

		//printf("%ld.%ld\n\n", transTime.tv_sec, transTime.tv_usec);

		//calcula os deltas
		long int diff = time_diff(transTime, timeStampsRec[k]);		
		countDeltas += diff;
		//printf("Delta %d is %ld\n\n", k+1, diff);
	}

	float meanDelta = countDeltas/(timeStampsRec.size()-1);
	meanDelta = meanDelta/1000;
	printf("Mean delta is: %f milisseconds\n\n", meanDelta);
}


